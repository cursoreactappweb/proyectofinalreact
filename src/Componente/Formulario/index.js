import React,{useState} from 'react';
import Select from "react-select";
import { Formik, Form, Field,Label } from "formik";
import { useHistory } from "react-router-dom";

const Formulario = ({ id, options, onChange, len }) => {
  const data = options.map(opt => ({
    label: opt.nombreOpcion,
    value: opt.esVerdadera
  }));

  //const [cont, setCont] = useState(0);
 //const onSetCont = (val) => setCont(val);

 const history = useHistory();

  const [respuesta, setRespuesta] = useState();
  const onSetRespuesta = val => setRespuesta(val);

  
 
 

  const SelectField = props => {
    return (
      <Select
        options={props.options}
        {...props.field}
        onChange={option => {
          props.form.setFieldValue(props.field.name, option);
          onSetRespuesta(option.value);
          //console.log("cont--",cont);
          //option.value&&onSetCont(parseInt(cont)+1);   
           option.value&&localStorage.setItem("respCorrecta", parseInt(localStorage.getItem("respCorrecta")) + parseInt(1));
  }}
      />
    )
  }

  

  return (
    <Formik initialValues={{ data, select: "" }}
     
    onSubmit={() => {

                    if (parseInt(id)+1!==len) {
                        onChange(parseInt(id) + 1);
                    }
                    else {
                       
                        history.push("/resultado");
                    }
                    //localStorage.setItem("respCorrecta", cont);
                }
                }

    >
      {({ values, handleSubmit }) => 
        <Form onSubmit={handleSubmit}>
          <Field name="select" options={values.data} component={SelectField} />
          <label>Respuesta: {respuesta ? "Verdadera" : "Falsa"}</label>
           {(parseInt(id)+1)===len?<button>Finalizar</button>:<button>Siguiente</button>}
        </Form>
      }
    </Formik>
  );
};

export default Formulario;
