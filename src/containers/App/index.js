// @vendors
import React,{useState} from "react";
import {  Switch, Route, Link,useHistory } from 'react-router-dom';
// @components

import Login from "../Login";
import Home from '../Home';
import { useAuth } from "../../Utilities/useAuth";
import "./styles.css";
import validarSesion from "../../Utilities/validarSesion";
import {Redirect } from 'react-router-dom';
import Container from "../../Componente/Container"
import Formulario from "../../Componente/Formulario"
export const AuthContext = React.createContext();

export default function App() {

   const sesion = validarSesion();
   console.log("sesion App " + sesion);
   const history = useHistory();
   const auth = useAuth();

const [next, setNext] = useState(0);
const onNext = val => {setNext(val)}

const contenidoFormulario = [{
    "pregunta" : "la suma de 2 mas 2 es:" ,
    "opciones" : [
        {
            "nombreOpcion" : "4" ,
            "esVerdadera" : true
        },
        {
            "nombreOpcion" : "3" ,
            "esVerdadera" : false
        }
    ]
},
    {
        "pregunta" : "la multiplicacion de 2 *2:" ,
        "opciones" : [
            {
                "nombreOpcion" : "4" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "3" ,
                "esVerdadera" : false
            }
        ]
    },
    {
        "pregunta" : "la division de 2 / 2 es:" ,
        "opciones" : [
            {
                "nombreOpcion" : "1" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "0" ,
                "esVerdadera" : false
            }
        ]
    },
    {
        "pregunta" : "la resta de 2 - 2 es:" ,
        "opciones" : [
            {
                "nombreOpcion" : "0" ,
                "esVerdadera" : true
            },
            {
                "nombreOpcion" : "1" ,
                "esVerdadera" : false
            }
        ]
    }
 
    ];

    const Resultado = () => {
   return(
    <div>
        <h3>Resultados</h3>
        {<label>De  {contenidoFormulario.length} preguntas en la encuesta, usted contesto {localStorage.getItem("respCorrecta")&&localStorage.getItem("respCorrecta")} correctamente </label>}
    </div>
   );
};

  return (
   
    <div className="layout">
  <nav className="layout-nav">
     
        <div>
          <div className="header">
          <ul>
            <li>
            <Link  to="/">Home</Link>
            </li>
            
               <li>
            {sesion ? (
              <button
                onClick={() => {
                  auth.logout(() => history.push("/login"));
                   
                }}
              >
                Cerrar Session
              </button>
            ) : (
              <Link to="/login">Iniciar Session</Link>
            
              
            )
              
        
          }
          </li>

          </ul>
          </div>
          <div className="content">
            <Switch>
           
            <Route path="/login" component={Login} />  
              <Route path="/resultado" component={Resultado} />
           <Route path="/"
                    component={() =>
                      sesion ? 
                    <Container title="Encuesta">
                     <label>Responder las siguientes preguntas: </label>
                    { 
                          
                         <Container key={next} title={contenidoFormulario[next].pregunta}>
                        <Formulario id={next} options={contenidoFormulario[next].opciones} 
                        onChange={onNext} len={contenidoFormulario.length} />
                        </Container>

                      }
                     </Container>
                   
                       
                      : <Redirect to = "/login" />
                    }
                  />
                  
            </Switch>
          </div>
        </div>
   
</nav>
    </div>
   
  );
}
 

