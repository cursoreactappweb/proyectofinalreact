import React from 'react';
import validarSesion from "../../Utilities/validarSesion";
import {Redirect } from 'react-router-dom';

export default () => {
  const sesion = validarSesion();
  console.log("sesion Home "+sesion);
  if (!sesion) return <Redirect to="/login" />;
  return (
    <div>
      Bienvenido a la página Home!
    </div>
  );
};
 
