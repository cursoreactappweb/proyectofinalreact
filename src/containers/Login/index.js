
import React, { useState } from 'react';
 import axios from "axios";
 import { useHistory,Redirect } from "react-router-dom";
 import { Wrapper } from "./estilo";
import { useAuth } from "../../Utilities/useAuth";
import validarSesion from "../../Utilities/validarSesion";



//function Login() {
  export default () => {
const sesion = validarSesion();
if (sesion) return <Redirect to = "/login" / > ;

const history = useHistory();
  const auth = useAuth();
   



const [isAuthenticated, setIsAuthenticated] = useState(sesion);

  
 const [username, setUsername] = useState("");
 const [password, setPassword] = useState("");
  

  
  const handleLogin = () => {
    const user = {username:username,password:password}
     localStorage.setItem("username", user.username);
     localStorage.setItem("password", user.password);
     setIsAuthenticated(true); //modifico el estado
    console.log("handleLogin "+ user.username+" "+user.password);
    login(user, () => history.push('/'));
      
  }
 


 const login = async (user, callback) => {
 

   console.log("hola axios");
   const res = await axios.post(
     "https://login-test-dga.herokuapp.com/login",
     user
   );

   if (res.data.response) {
   callback();
   }

    
 };


  return (
    <Wrapper>
      Login<br /><br />
      <div>
        Username<br />
        <input type="text" value={username} onChange={(e)=>{
          setUsername(e.target.value);
        }}/>
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" value={password} onChange={(e)=>{
          setPassword(e.target.value);
        }} />
      </div>
      
      <input type="button" value="Login" onClick={handleLogin}  /><br />
    </Wrapper>
  );
};
 

