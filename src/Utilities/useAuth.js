//import React, { useContext, useState, createContext } from "react";
import React, { useContext, useState, createContext } from "react";
import validarSesion from "./validarSesion";

const Context = createContext({});
//const AuthContext = React.createContext();
export const AuthProvider = props => {
  
  const sesion = validarSesion();
  console.log("sesion en useAuth: "+sesion);
  const [isAuthenticated, setIsAuthenticated] = useState(sesion);

  console.log("isAuthenticated useAuth " + isAuthenticated);

  const login = (fields, callback) => {
    localStorage.setItem("username", fields.username);
    localStorage.setItem("password", fields.password);
    setIsAuthenticated(true); //modifico el estado
    callback();
  };

  const logout = callback => {
    localStorage.setItem("username", "");
    localStorage.setItem("password", "");
    localStorage.setItem("respCorrecta", 0);
    setIsAuthenticated(false); //modifico el estado
    callback();
  };

  return (
    <Context.Provider value={{ isAuthenticated, login, logout }}>
      {props.children}
    </Context.Provider>
  );
};

export const useAuth = () => {
  return useContext(Context);
};