import React from "react";
import ReactDOM from "react-dom";

import App from "./containers/App";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider} from "./Utilities/useAuth";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
    <AuthProvider>
      <App />
    </AuthProvider>
  </BrowserRouter>,
  rootElement
);